package log

import (
	"log"
	"os"
	"fmt"
	"path"
	"time"
	"sync"
)

const (
	file_max_line = 10*10000
	)

var is_log_file bool
var is_log_out bool

var log_ti time.Time
var debug FileLog
var info FileLog
var warn FileLog
var errors FileLog

type FileLog struct {
	file *os.File
	logger *log.Logger
	filename string
	write_line int
	file_idx int
	log_ti time.Time
	lock sync.Mutex
}

func (f *FileLog) Init(filename string) {
	f.filename = filename
	err := f.open_log(filename, true)
	if err != nil {
		log.Fatal(err)
	}
}
func (f *FileLog) SetLogTimeFlag(show bool) {
	fg := 0
	if show {
		fg = log.LstdFlags
	}
	f.logger.SetFlags(fg)
}
func (f *FileLog) SetLogTime(ti time.Time) {
	f.log_ti = ti
}
func (f *FileLog) Println(v ...interface{}) {
	f.lock.Lock()
	defer f.lock.Unlock()

	if f.filename == "" {
		return
	}

	if f.file == nil {
		f.check_open()
		if f.file == nil {
			return
		}
	}

	if !f.log_ti.IsZero() {
		s := fmt.Sprintln(v...)
		f.logger.Printf("%s %s", f.log_ti.Format("2006/01/02 15:04:05"), s)
	} else {
		f.logger.Println(v...)
	}

	f.write_line++

	if f.write_line > file_max_line {
		f.check_new_file()
	}
}

func (f *FileLog) Printf(format string, v ...interface{}) {
	f.lock.Lock()
	defer f.lock.Unlock()

	if f.filename == "" {
		return
	}

	if f.file == nil {
		f.check_open()
		if f.file == nil {
			return
		}
	}

	if !f.log_ti.IsZero() {
		s := fmt.Sprintf(format, v...)
		f.logger.Printf("%s %s", f.log_ti.Format("2006/01/02 15:04:05"), s)
	} else {
		f.logger.Printf(format, v...)
	}

	f.write_line++

	if f.write_line > file_max_line {
		f.check_new_file()
	}
}

func (f *FileLog) check_open() {
	if f.file != nil {
		return
	}

	err := f.open_log(f.filename, false)
	if err != nil {
		return
	}
}

func (f *FileLog) check_new_file() {
	if f.write_line < file_max_line {
		return
	}

	f.file.Close()
	f.file = nil
	f.logger = nil

	filename := f.filename

	ext := path.Ext(filename)
	orgname := filename[:len(f.filename)-len(ext)]

	f.file_idx++
	f.file_idx %= 2001
	if f.file_idx < 1 {
		f.file_idx = 1
	}

	idx := f.file_idx
	newname := fmt.Sprintf("%s%d%s", orgname, idx, ext)

	err := os.Rename(filename, newname)
	if err != nil {
		fmt.Printf("log rename %s to %s failed %s\n", filename, newname, err)
		return
	}

	err = f.open_log(filename, false)
	if err != nil {
		fmt.Println(err)
		return
	}

	f.write_line = 0
}

func (f *FileLog) open_log(name string, is_append bool) error {
	flag := os.O_WRONLY|os.O_CREATE
	if is_append {
		flag = flag | os.O_APPEND
	} else {
		flag = flag | os.O_TRUNC
	}
	file, err := os.OpenFile(name, flag, 0666)
	if err != nil {
		return err
	}
	logger := log.New(file, "", log.LstdFlags)
	f.file = file
	f.logger = logger
	return nil
}


func Init(log_file bool, log_out bool) {
	is_log_file = log_file
	is_log_out = log_out
	log.SetOutput(os.Stdout)
	if is_log_file {
		os.Mkdir("log", 666)

		debug.Init("log/debug.txt")
		info.Init("log/info.txt")
		warn.Init("log/warn.txt")
		errors.Init("log/error.txt")
	}
}
// 日志是否显示时间
func SetLogTimeFlag(show bool) {
	fg := 0
	if show {
		fg = log.LstdFlags
	}
	log.SetFlags(fg)

	if is_log_file {
		debug.SetLogTimeFlag(show)
		info.SetLogTimeFlag(show)
		warn.SetLogTimeFlag(show)
		errors.SetLogTimeFlag(show)
	}
}

func SetLogTime(ti time.Time) {
	log_ti = ti
	if is_log_file {
		debug.SetLogTime(ti)
		info.SetLogTime(ti)
		warn.SetLogTime(ti)
		errors.SetLogTime(ti)
	}
}
func printlogln(v ...interface{}) {
	if !is_log_out {
		return
	}

	if !log_ti.IsZero() {
		s := fmt.Sprintln(v...)
		log.Printf("%s %s", log_ti.Format("2006/01/02 15:04:05"), s)
		return
	}

	log.Println(v...)
}
func printlog(format string, v ...interface{}) {
	if !is_log_out {
		return
	}

	if !log_ti.IsZero() {
		s := fmt.Sprintf(format, v...)
		log.Printf("%s %s", log_ti.Format("2006/01/02 15:04:05"), s)
		return
	}

	log.Printf(format, v...)
}
func Infoln(v ...interface{}) {
	info.Println(v...)
	printlogln(v...)
}
func Info(format string, v ...interface{}) {
	info.Printf(format, v...)
	printlog(format, v...)
}

func Warnln(v ...interface{}) {
	warn.Println(v...)
	printlogln(v...)
}
func Warn(format string, v ...interface{}) {
	warn.Printf(format, v...)
	printlog(format, v...)
}

func Debugln(v ...interface{}) {
	debug.Println(v...)
	printlogln(v...)
}
func Debug(format string, v ...interface{}) {
	debug.Printf(format, v...)
	printlog(format, v...)
}

func Errorln(v ...interface{}) {
	errors.Println(v...)
	printlogln(v...)
}
func Error(format string, v ...interface{}) {
	errors.Printf(format, v...)
	printlog(format, v...)
}
